<?php
get_header();
$args = array(
  'post_type' => 'events',
  'orderby' => 'meta_value',
  'meta_key' => 'start_time',
  'order' => 'ASC',
  'post_status' => 'publish',
  'showposts' => 0,
  'offset' => 0,
);
$the_loop =  new WP_Query($args);
?>

<body>
  <h1><a href="/">HOME</a></h1>
  <div id="app">
    <h2>絞り込み検索</h2>
    <?php if (function_exists('create_searchform')) { ?>
      <?php global $wp_query; ?>
      <div id="feas-0">
        <div id="feas-form-0">
          <form id='feas-searchform-0' action='http://calcioclone.local/' method='get'>
            <fieldset class="uk-fieldset">
              <div class="uk-margin">
                <div>ジャンル</div>
                <label id='feas_0_0_0'><input type='radio' name='search_element_0' value='個サル' class="uk-radio" />個サル</label>
                <label id='feas_0_0_1'><input type='radio' name='search_element_0' value='ソサイチ' class="uk-radio" />ソサイチ</label>
                <label id='feas_0_0_2'><input type='radio' name='search_element_0' value='フットサル個人参加型大会' class="uk-radio" />フットサル個人参加型大会</label>
              </div>
              <div class="uk-margin">
                <div>会場</div>
                <select name='search_element_1' id='feas_0_1'>
                  <option id='feas_0_1_none' value=''></option>
                  <option id='feas_0_1_0' value='中央区総合スポーツセンター（屋内コート）第２競技場'>中央区総合スポーツセンター（屋内コート）第２競技場</option>
                  <option id='feas_0_1_0' value='中央区総合スポーツセンター（屋内コート）主競技場'>中央区総合スポーツセンター（屋内コート）主競技場</option>
                  <option id='feas_0_1_0' value='フットサルパーク吉祥寺（屋外コート）'>フットサルパーク吉祥寺（屋外コート）</option>
                  <option id='feas_0_1_0' value='カルチョスタジアム本八幡（屋外コート）'>カルチョスタジアム本八幡（屋外コート）</option>
                </select>
              </div>
              <div class="uk-margin">
                <div>開催日</div>
                <input name='search_element_2' id='feas_0_2' name="date" type="date" />
              </div>
              <div class="uk-margin">
                <div>キーワードで探す</div>
                <input type='text' name='s_keyword_3' id='feas_0_3' value='' class="uk-input" placeholder="2時間 3時間 etc" />
              </div>

              <div class="uk-margin">
                <input type='submit' name='searchbutton' id='feas-submit-button-0' class='feas-submit-button' value='検索' />
              </div>
              <!--  -->
              <!--  -->
              <input type='hidden' name='csp' value='search_add' />
              <input type='hidden' name='feadvns_max_line_0' value='4' />
              <input type='hidden' name='fe_form_no' value='0' />
            </fieldset>
          </form>
        </div>
        <div id="feas-result-0">
          <?php if (is_search()) { ?>
            <?php if ($add_where != null || $w_keyword != null) : ?>
              「<?php search_result(); ?>」の条件による検索結果 <?php feas_count_posts(); ?> 件
            <?php else : ?>
              <h3>検索条件が指定されていません。</h3>
            <?php endif; ?>
          <?php } ?>

        </div>
      </div>
    <?php } ?>
    <h2 class="uk-heading-line uk-text-center"><span>スケジュール</span></h2>
    <div class="events_wrapper">
      <table class="uk-table uk-table-justify uk-table-divider">
        <thead>
          <tr>
            <th class="uk-width-small">日時</th>
            <th>会場</th>
            <th class="uk-width-small">ジャンル</th>
            <th>カテゴリ</th>
            <th>料金</th>
            <th>参加状況</th>
            <th>詳細</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if ($the_loop->post_count !== 0) :
            while ($the_loop->have_posts()) :
              $the_loop->the_post();
              $custom_fields = $the_loop->post;
              $category_term = get_the_terms($the_loop->id, 'events-category')[0];

          ?>
              <tr>
                <td>
                  <p><?php echo $custom_fields->start_time; ?>~ <br>
                    <?php echo $custom_fields->end_time; ?>
                  </p>
                </td>
                <td>
                  <p><?php echo $custom_fields->venue; ?></p>
                </td>
                <td>
                  <p><?php echo $custom_fields->genre ?></p>
                </td>
                <td>
                  <p><?php echo $category_term->name ?></p>
                </td>
                <td>
                  <p>男性:<?php echo $custom_fields->price_men ?>円</p>
                  <p>女性:<?php echo $custom_fields->price_women ?>円</p>
                </td>
                <td>
                  <p><?php echo $custom_fields->participation_status; ?></p>
                </td>
                <td>
                  <button class="uk-button uk-button-default" type="button">
                    <a href="<?php the_permalink(); ?>">詳細</a>
                  </button>
                </td>
              </tr>
          <?php
            endwhile;
          endif;
          wp_reset_postdata();
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <script type="text/javascript">
  </script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
  <?php wp_footer(); ?>
</body>
