<?php
require_once(get_template_directory() . '/modules/custom-post-type.php');

// カスタムフィールドをAPIに含む
add_action('rest_api_init', 'add_custom_fields_to_rest');
function add_custom_fields_to_rest()
{
  register_rest_field(
    'events',
    'custom_fields',
    [
      'get_callback'    => 'get_custom_fields_value', // カスタム関数名指定
      'update_callback' => null,
      'schema'          => null,
    ]
  );
  function get_custom_fields_value()
  {
    return get_post_custom();
  }
}
