<?php

/**
 * カスタム投稿タイプ：イベント
 * events
 */
add_action('init', 'add_events_type');
function add_events_type()
{
  $args = array(
    'label' => 'イベント情報',
    'labels' => array(
      'singular_name' => 'イベント名',
      'add_new_item' => 'イベントを登録',
      'add_new' => '新規追加',
      'new_item' => '新規商イベント',
      'view_item' => 'イベントを表示',
      'not_found' => 'イベントは見つかりませんでした。',
      'not_found_in_trash' => 'ゴミ箱にイベントはありませんでした',
      'search_items' => 'イベントを検索',
    ),
    'public' => true,
    'order' => 'asc',
    'has_archive' => true,
    'show_in_rest' => true,
    'rest_base' => 'events',
    'hierarchical' => false,
    'menu_position' => 6,
    'supports' => array('title', 'thumbnail', 'custom-fields')

  );

  //カスタムタクソノミー、タグタイプ
  register_taxonomy(
    'events-tag',
    'events',
    array(
      'hierarchical' => false,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'イベントのタグ',
      'singular_label' => 'イベントのタグ',
      'public' => true,
      'show_ui' => true
    )
  );
  //カスタムタクソノミー：カテゴリー
  register_taxonomy(
    'events-category',
    'events',
    array(
      'hierarchical' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'イベントのカテゴリー',
      'singular_label' => 'イベントのカテゴリー',
      'public' => true,
      'show_ui' => true,
      'show_in_rest' => true,
    )
  );
  register_post_type('events', $args);
}
