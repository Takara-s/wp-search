"use strict";
new Vue({
  el: "#app",
  data: {
    day: ["日", "月", "火", "水", "木", "金", "土"],
    events: events,
  },
  computed: {
    //
  },

  methods: {
    jumpDetails(link) {
      window.location.href = link;
    },
    startDateTime: function (startDateTime) {
      let date = new Date(startDateTime);
      let startMonth = date.getMonth() + 1;
      let startDate = ("0" + date.getDate()).slice(-2);
      let startDay = this.day[date.getDay()];
      let startHour = ("0" + date.getHours()).slice(-2);
      let startMin = ("0" + date.getMinutes()).slice(-2);
      return [
        `${startMonth}月${startDate}日(${startDay})`,
        ` ${startHour}:${startMin}~`,
      ];
    },
    endDateTime: function (endDateTime) {
      let date = new Date(endDateTime);
      let endHour = ("0" + date.getHours()).slice(-2);
      let endMin = ("0" + date.getMinutes()).slice(-2);
      return `${endHour}:${endMin}`;
    },
  },
  // mounted: function mounted() {
  //   const self = this;
  //   axios
  //     .get("http://calcioclone.local/wp-json/wp/v2/events")
  //     .then(function (res) {
  //       self.events = res.data;
  //     });
  // },
});
